/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Player.hpp>
#include <Shot.hpp>
#include <random>
#include <chrono>

Player::Player(Core* core, LevelScene* level, sk::Vector2f position)  
  : LevelActor(core, level, position)
{
  sf::Texture* t = m_core->textures()->get("player");
  
  m_shot = std::make_unique<Shot>(m_core, m_level, this, 500.0);
  
  m_shape.setTexture(t);
}

/*virtual*/ void Player::update()
{
  if (!m_alive) { return; }

  float x_vel = 0.0f;
  float y_vel = 0.0f;
    
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)
      || sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
    {
      y_vel = -m_velocity;
      if ( m_level->collision(this) ) { y_vel = 0.0f; }
    }
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
    {
      y_vel = m_velocity;
      if ( m_level->collision(this) ) { y_vel = 0.0f; }
    }
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q)
      || sf::Keyboard::isKeyPressed(sf::Keyboard::A))
    {
      x_vel = -m_velocity;
      if ( m_level->collision(this) ) { x_vel = 0.0f; }
    }
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
      x_vel = m_velocity;
      if ( m_level->collision(this) ) { x_vel = 0.0f; }
    }
  
  if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
    {
      m_shot->fire( m_position, m_core->mouse() );
    }

  if( m_core->cheating() )
    {
      m_shot = std::make_unique<Shot>(m_core, m_level, this, 0);
    }
  
  // compute direction
  lookAt( m_core->mouse() );

  move(x_vel, y_vel);
  
  LevelActor::update();
}

/*virtual*/ void Player::display()
{
  if (!m_alive) { return; }
  LevelActor::display();
}

void Player::die ()
{
  LevelActor::die();
  m_core->goFailure();
}

/*virtual*/ void Player::takeShot()
{
  std::mt19937 rng(std::chrono::steady_clock::now().time_since_epoch().count());
  std::uniform_real_distribution<float> urd_damage(m_max_life/3.0
						   , 2.0*m_max_life/3.0);

  m_current_life -= urd_damage(rng);
}

Player::~Player()
{
  
}
