/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <MainScene.hpp>

MainScene::MainScene(Core* core)
  : PictureScene(core, "main")
{
}

/*virtual*/ void MainScene::reset()
{
  m_core->playMusic("enter_the_room");
  PictureScene::reset();
}

/*virtual*/ void MainScene::update()
{
  if ( accept()
      && ready() )
    {
      m_core->goLevel();
      m_clock.restart();
    }
  
  if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)
       && ready() )
    {
      m_core->window()->close();
      m_clock.restart();
    }
  
  if ( sf::Keyboard::isKeyPressed(sf::Keyboard::H)
       && ready() )
    {
      m_core->goHelp();
      m_clock.restart();
    }

}

MainScene::~MainScene()
{
  
}
