/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <PictureScene.hpp>

PictureScene::PictureScene(Core* core, std::string picture_name)
  : Scene(core)
  , m_delay(500.0)
{
    m_bg.setSize(sf::Vector2f(m_core->window()->getSize()));
    m_bg.setTexture(m_core->textures()->get(picture_name));
}

/*virtual*/ bool PictureScene::ready()
{
  return m_clock.getElapsedTime().asMilliseconds() >= m_delay;
}

/*virtual*/ void PictureScene::reset()
{
  m_clock.restart();
}

/*virtual*/ void PictureScene::display()
{
  m_core->window()->draw(m_bg);
}

/*virtual*/ bool PictureScene::accept()
{
  return sf::Keyboard::isKeyPressed(sf::Keyboard::Return)
    || sf::Keyboard::isKeyPressed(sf::Keyboard::Space);
}

PictureScene::~PictureScene()
{
  
}
