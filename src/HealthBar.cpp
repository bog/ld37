/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <HealthBar.hpp>

HealthBar::HealthBar(Core* core, sk::Vector2f position, sk::Vector2f size, float max)
  : Scene(core)
  , m_max(max)
  , m_current(m_max)
  , m_position(position)
  , m_size(size)
{
  m_shape.setFillColor(sf::Color::Black);
}

/*virtual*/ void HealthBar::update()
{
  m_shape.setPosition(sf::Vector2f(m_position.x() - m_size.x()/2, m_position.y() - m_size.y()/2));
  m_shape.setSize(sf::Vector2f(m_size.x(), m_size.y()));

  float ratio = m_current/m_max;
  if(m_max == 0) { throw "healthbar : div par zero"; }

  m_bar.setPosition(m_shape.getPosition());
  m_bar.setSize(sf::Vector2f(m_shape.getSize().x * ratio, m_shape.getSize().y));

  if (ratio > 80)
    {
      m_bar.setFillColor(sf::Color::Green);
    }
  else if (ratio > 50)
    {
      m_bar.setFillColor(sf::Color(200, 200, 20));
    }
  else if (ratio >20)
    {
      m_bar.setFillColor(sf::Color(200, 80, 20));
    }
  else
    {
      m_bar.setFillColor(sf::Color::Red);
    }
}

/*virtual*/ void HealthBar::display()
{
  m_core->window()->draw(m_shape);
  m_core->window()->draw(m_bar);
}


HealthBar::~HealthBar()
{
  
}
