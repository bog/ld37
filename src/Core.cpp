/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Core.hpp>
#include <LevelScene.hpp>
#include <PictureScene.hpp>
#include <EndScene.hpp>
#include <FailureScene.hpp>
#include <MainScene.hpp>
#include <HelpScene.hpp>
#include <random>
#include <chrono>

Core::Core(sf::RenderWindow* window)
  : m_window(window)
  , m_current_scene (nullptr)
  , m_current_music (nullptr)
{
  loadResources();
  
  m_end_scene = std::make_unique<EndScene>(this);
  m_failure_scene = std::make_unique<FailureScene>(this);
  m_main_scene = std::make_unique<MainScene>(this);
  m_help_scene = std::make_unique<HelpScene>(this);
  goMain();
}

void Core::loadResources ()
{
  // Textures
  m_textures = std::make_unique<sk::ResourceCache<sf::Texture>>();
  m_textures->add("player", new sk::TextureResource("../assets/img/player.png"));
  m_textures->add("slurr", new sk::TextureResource("../assets/img/slurr.png"));
  m_textures->add("spawn", new sk::TextureResource("../assets/img/spawn_point.png"));
  m_textures->add("goal", new sk::TextureResource("../assets/img/goal.png"));
  m_textures->add("goal_closed", new sk::TextureResource("../assets/img/goal_closed.png"));
  m_textures->add("bg", new sk::TextureResource("../assets/img/bg.png"));
  m_textures->add("end", new sk::TextureResource("../assets/img/end.png"));
  m_textures->add("failure", new sk::TextureResource("../assets/img/failure.png"));
  m_textures->add("main", new sk::TextureResource("../assets/img/main.png"));
  m_textures->add("help", new sk::TextureResource("../assets/img/help.png"));
  m_textures->add("wall", new sk::TextureResource("../assets/img/wall.png"));
  m_textures->add("splash", new sk::TextureResource("../assets/img/splash.png"));

  // Sound
  m_sound_buffers = std::make_unique<sk::ResourceCache<sf::SoundBuffer>>();
  m_sound_buffers->add("snd_shoot", new sk::SoundBufferResource("../assets/snd/shoot.ogg"));

  // Music
  m_musics = std::make_unique<sk::ResourceCache<sf::Music>>();
  m_musics->add("enter_the_room", new sk::MusicResource("../assets/music/enter_the_room.ogg"));
  m_musics->add("behind_a_wall", new sk::MusicResource("../assets/music/behind_a_wall.ogg"));
  m_musics->add("fighting", new sk::MusicResource("../assets/music/fighting.ogg"));
  m_musics->add("so_close", new sk::MusicResource("../assets/music/so_close.ogg"));
}

void Core::go(std::unique_ptr<Scene>& scene)
{
  if(m_current_music)
    {
      m_current_music->stop();
    }
  
  m_current_scene = scene.get();
}

void Core::reset(std::unique_ptr<Scene>& scene)
{
  PictureScene* ps = static_cast<PictureScene*>( scene.get() );
  ps->reset();
}

void Core::goLevel()
{
  m_level_scene = std::make_unique<LevelScene>(this);
  go(m_level_scene);
  playRandomGameMusic();
}

void Core::goEnd()
{
  go(m_end_scene);
  reset(m_end_scene);
}

void Core::goFailure()
{
  go(m_failure_scene);
  reset(m_failure_scene);
}

void Core::goMain()
{  
  go(m_main_scene);
  reset(m_main_scene);
}

void Core::goHelp()
{
  go(m_help_scene);
  reset(m_help_scene);
}

void Core::playRandomGameMusic()
{
  // Playlist.
  std::mt19937 rng(std::chrono::steady_clock::now().time_since_epoch().count());
  std::uniform_int_distribution<int> uid_music (1, 3);

  switch( uid_music(rng) )
    {
    case 1:
      playMusic("behind_a_wall");
      break;
    case 2:
      playMusic("fighting");
      break;
    case 3:
      playMusic("so_close");
      break;
    }
}

void Core::update()
{
  if (m_current_scene)
    {
      m_current_scene->update();
    }
}

void Core::display()
{
  if (m_current_scene)
    {
      m_current_scene->display();
    }
}

void Core::run()
{
  sf::Event event;

  while ( m_window->isOpen() )
    {
      m_framerate.begin();
      
      while ( m_window->pollEvent(event) )
	{
	  switch(event.type)
	    {
	    case sf::Event::Closed:
	      m_window->close();
	      break;
	    default:break;
	    }
	}

      update();
      m_window->clear(sf::Color::White);
      display();
      m_window->display();

      m_framerate.end();
    }
}

Core::~Core()
{
  
}
