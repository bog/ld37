/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Wall.hpp>

Wall::Wall(Core* core, sk::Vector2f pos, sk::Vector2f size)
  : Scene(core)
  , m_position(pos)
  , m_size(size)
{
  m_shape.setTexture(m_core->textures()->get("wall"));
}

/*virtual*/ void Wall::update()
{
  m_shape.setPosition(sf::Vector2f(m_position.x() - m_size.x()/2.0f
				   , m_position.y() - m_size.y()/2.0f));
  
  m_shape.setSize(sf::Vector2f(m_size.x(), m_size.y()));
}

/*virtual*/ void Wall::display()
{
  m_core->window()->draw(m_shape);
}

Wall::~Wall()
{
  
}
