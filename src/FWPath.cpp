/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <FWPath.hpp>

const int FWPath::MAX_COST = 1000;

FWPath::FWPath(LevelScene* level, LevelGrid& grid)
  : m_level (level)
{
  m_n_vertex = grid.size() * grid[0].size();

  // Build cost and paths matrice.
  for(size_t s1=0; s1<m_n_vertex; s1++)
    {
      m_costs.push_back( std::vector<float>() );
      m_paths.push_back( std::vector<int>() );
  
      for(size_t s2=0; s2<m_n_vertex; s2++)
	{
  	  m_costs[s1].push_back(FWPath::MAX_COST);
	  m_paths[s1].push_back(FWPath::MAX_COST);
	}
    }

  // Fill the matricies
  for(size_t s1=0; s1<m_n_vertex; s1++)
    {
      
      size_t s1_i = s1/grid.size();
      size_t s1_j = s1%grid.size();
      
      for(size_t s2=0; s2<m_n_vertex; s2++)
	{
	  size_t s2_i = s2/grid.size();
	  size_t s2_j = s2%grid.size();

	  
	  if (s1 == s2)
	    {
	      m_costs[s1][s2] = 0;
	      m_paths[s1][s2] = s2;
	    }
	  
	  // The vertex are neighbors.
	  else if ( ((int)(s1_i)-(int)(s2_i))
	       * ((int)(s1_i)-(int)(s2_i))
	       + ((int)(s1_j)-(int)(s2_j))
	       * ((int)(s1_j)-(int)(s2_j)) == 1 )
	    {

	      // s1 is accessible from s2
	      if ( grid[s1_i][s1_j].second != 'w')
	      	{
	      	  m_costs[s2][s1] = 1.0;
	      	  m_paths[s2][s1] = s2;
	      	}

	      // s2 is accessible from s1
	      if ( grid[s2_i][s2_j].second != 'w')
		{
		  m_costs[s1][s2] = 1.0;
		  m_paths[s1][s2] = s1;
		}
	    }
	  else
	    {
	      m_costs[s1][s2] = FWPath::MAX_COST;
	      m_costs[s2][s1] = FWPath::MAX_COST;
	      m_paths[s1][s2] = FWPath::MAX_COST;
	      m_paths[s2][s1] = FWPath::MAX_COST;
	    }
	}
    }
}

void FWPath::compute()
{
  for(size_t s1=0; s1<m_n_vertex; s1++)
    {
      for(size_t s2=0; s2<m_n_vertex; s2++)
	{
	  for(size_t s3=0; s3<m_n_vertex; s3++)
	    {
	      if(s2 == s3 || s1 == s2 || s1 == s3)
	      	{
	      	  continue;
	      	}

	      if ( m_costs[s2][s1] == FWPath::MAX_COST
	      	   && m_costs[s1][s3] == FWPath::MAX_COST )
	      	{
	      	  continue;
	      	}
	      
	      // Shorter path found
	      // s2 -> s1 -> s3
	      if ( m_costs[s2][s3] 
		   > m_costs[s2][s1]
		   + m_costs[s1][s3] )
		// s2-s3 shorter
		{
		  m_costs[s2][s3] = m_costs[s2][s1] + m_costs[s1][s3];
		  m_paths[s2][s3] = m_paths[s1][s3];
		}
	    }
	}
    }
}

FWPath::~FWPath()
{
  
}
