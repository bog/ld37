/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Goal.hpp>

Goal::Goal(Core* core, sk::Vector2f position)
  : LevelEntity(core, position)
  , m_open(false)
{
  radius(48.0);  
  close();
}

void Goal::open ()
{
  m_shape.setTexture(m_core->textures()->get("goal"));
  m_open = true;
}

void Goal::close ()
{
  m_shape.setTexture(m_core->textures()->get("goal_closed"));
  m_open = false;
}

/*virtual*/ void Goal::update()
{
  LevelEntity::update();
}

/*virtual*/ void Goal::display()
{
  LevelEntity::display();
}


Goal::~Goal()
{
  
}
