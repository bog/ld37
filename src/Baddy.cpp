/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Baddy.hpp>
#include <Player.hpp>
#include <Shot.hpp>
#include <random>
#include <chrono>
#include <FWPath.hpp>

Baddy::Baddy(Core* core, LevelScene* level, sk::Vector2f position)
  : LevelActor(core, level, position)
  , m_accuracy(128)    
{
  m_shape.setTexture(m_core->textures()->get("slurr"));

  std::mt19937 rng(std::chrono::steady_clock::now().time_since_epoch().count());
  std::uniform_real_distribution<float> urd_dist_player(3, 6);

  m_max_dist_player = urd_dist_player(rng) * m_radius * 2;
  m_max_dist_player = m_radius*2;
  m_current_step = m_current_vertex;
}

/*virtual*/ void Baddy::update()
{
  if (!m_alive) { return; }

  if ( (m_position - m_level->player()->position()).length() > m_max_dist_player )
    {
      sk::Vector2f current_step_pos = m_level->vertexPosition(m_current_step);
      //lookAt( current_step_pos );
      sk::Vector2f current_step_dir = current_step_pos - m_position;
      
      if (current_step_dir.length() > m_radius)
	// moving the the current step
	{
	  move(current_step_dir.normalized().x(), current_step_dir.normalized().y());
	}
      else
	// next step
	{
	  FWPath* fwpath = m_level->path();
	  auto path = fwpath->paths();
	  auto costs = fwpath->costs();
	  
	  int target;

	  if ( costs[m_current_vertex][m_level->player()->currentVertex()] > 1 )
	    {
	      target = path[m_current_vertex][m_level->player()->currentVertex()];
	  
	      while ( costs[m_current_vertex][target] > 1 )
		{
		  target = path[m_current_vertex][target];

		}
	    }
	  else
	    {
	      target = m_level->player()->currentVertex();
	    }
	  
	  m_current_step = target;
	}
    }
  else
    {

    }
  lookAt( m_level->player()->position() );
  shootPlayer();
  //lookAt( m_level->player()->position() );
  //sk::Vector2f player_dir = m_level->player()->position() - m_position;
  
  // if (player_dir.length() > m_max_dist_player)
  //   // moving the the current step
  //   {
  //     move(player_dir.normalized().x(), player_dir.normalized().y());
  //   }
  // else
  //   // next step
  //   {
  //   }
  
  LevelActor::update();
}

/*virtual*/ void Baddy::shootPlayer()
{
  m_shot->fire( m_position, targetPoint(m_level->player()->position(), m_accuracy) );
}

/*virtual*/ sk::Vector2f Baddy::targetPoint (sk::Vector2f target, float accuracy)
{
  std::mt19937 rng(std::chrono::steady_clock::now().time_since_epoch().count());
  std::uniform_real_distribution<float> urd(-accuracy, accuracy);
  
  float rand_accuracy_x = urd(rng);
  float rand_accuracy_y = urd(rng);

  return {target.x() + rand_accuracy_x
      , target.y() + rand_accuracy_y};
}

/*virtual*/ void Baddy::takeShot()
{
  std::mt19937 rng(std::chrono::steady_clock::now().time_since_epoch().count());
  std::uniform_real_distribution<float> urd_damage(m_max_life/2.0
						   , 1.2 * m_max_life);

  m_current_life -= urd_damage(rng);
}

/*virtual*/ void Baddy::display()
{
  if (!m_alive) { return; }
  LevelActor::display();
}


Baddy::~Baddy()
{
  
}
