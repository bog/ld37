/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Shot.hpp>
#include <LevelScene.hpp>
#include <LevelActor.hpp>

Shot::Shot(Core* core, LevelScene* level, LevelActor* actor)
  : Shot(core, level, actor, 1300)
{

}

Shot::Shot(Core* core, LevelScene* level, LevelActor* actor, float cooldown)
  : Scene(core)
  , m_level(level)
  , m_fire_time(64.0)
  , m_shooting(false)
  , m_cooldown_time (cooldown)
  , m_actor(actor)
{
  m_array.setPrimitiveType(sf::Lines);
  m_range = m_core->window()->getSize().x + m_core->window()->getSize().y;
  m_sound.setBuffer(*m_core->soundBuffers()->get("snd_shoot"));
  m_sound.setVolume(20);
}

/*virtual*/ void Shot::update()
{
  if (m_shooting
      && m_fire_clock.getElapsedTime().asMilliseconds() >= m_fire_time)
    {

      m_shooting = false;
    }
}

/*virtual*/ void Shot::display()
{
  if(m_shooting)
    {
      m_core->window()->draw(m_array);
    }
}

void Shot::fire(sk::Vector2f source, sk::Vector2f destination)
{
  if (m_cooldown_clock.getElapsedTime().asMilliseconds() < m_cooldown_time)
    {
      return;
    }

  m_fire_clock.restart();
  m_shooting = true;
  m_source = source;
  m_destination = destination;

  sk::Vector2f direction = (destination - source).normalized();

  m_destination = source + ( direction * m_range);

  m_level->shotInterception(this);

  m_cooldown_clock.restart();
  
  m_array.clear();

  m_array.append( sf::Vertex( sf::Vector2f(m_source.x(), m_source.y())
			      , sf::Color::Yellow ) );

  m_array.append( sf::Vertex( sf::Vector2f(m_destination.x(), m_destination.y())
			      , sf::Color::Blue ) );


  m_sound.play();
}

LevelActor* Shot::actor()
{
  return m_actor;
}

Shot::~Shot()
{

}
