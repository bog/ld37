#include <iostream>
#include <SFML/Graphics.hpp>

#include <Core.hpp>

int main()
{
  sf::VideoMode mode = sf::VideoMode::getDesktopMode();
  mode.width *= 0.8;
  mode.height *= 0.8;
  
  sf::RenderWindow window (mode
			   , "Ludum dare 37"
			   , sf::Style::Titlebar
			   | sf::Style::Close);
  
  window.setFramerateLimit(60);
  
  Core core (&window);

  try
    {
      core.run();
    }
  catch (std::exception const& e)
    {
      std::cerr<< e.what() <<std::endl;
    }
  
  return 0;
}
