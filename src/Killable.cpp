/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Killable.hpp>

Killable::Killable(float max_life)
  : m_max_life(max_life)
  , m_current_life(m_max_life)
  , m_alive (true)

{
  
}

/*virtual*/ void Killable::die()
{
  m_current_life = 0;
  m_alive = false; 
}

Killable::~Killable()
{
  
}
