/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <HelpScene.hpp>

HelpScene::HelpScene(Core* core)
  : PictureScene(core, "help")
{
  
}

/*virtual*/ void HelpScene::reset()
{
  m_core->playMusic("behind_a_wall");
  PictureScene::reset();
}

/*virtual*/ void HelpScene::update()
{
  if ( accept()
      && ready() )
    {
      m_core->goMain();
    }
}

HelpScene::~HelpScene()
{
  
}
