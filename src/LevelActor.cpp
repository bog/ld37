/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <LevelActor.hpp>
#include <Shot.hpp>
#include <HealthBar.hpp>

float const LevelActor::MAX_LIFE = 100.0f;

LevelActor::LevelActor(Core* core, LevelScene* level, sk::Vector2f position)
  : LevelEntity(core, position)
  , Killable(LevelActor::MAX_LIFE)
  , m_level(level)
  , m_velocity(3)
  , m_angle (0.0f)
{
  radius( (m_core->window()->getSize().x/m_level->gridSize().x())/4.0 );
  m_shot = std::make_unique<Shot>(core, level, this);
  
  m_health_bar = std::make_unique<HealthBar>(core
					     , sk::Vector2f(m_position.x()
							    , m_position.y()
							    - m_radius)
					     , sk::Vector2f(m_radius*2
							    , m_radius/5)
					     , 100.0);

  updateCurrentVertex();
}

/*virtual*/ void LevelActor::move(float x_vel, float y_vel)
{
  m_position.x(m_position.x() + x_vel);  

  if ( m_level->collision(this) )
    {
      m_position.x(m_position.x() - x_vel);
    }

  m_position.y(m_position.y() + y_vel);

  if ( m_level->collision(this) )
    {
      m_position.y(m_position.y() - y_vel);
    }

  updateCurrentVertex();
}

void LevelActor::lookAt(sk::Vector2f point)
{
  float angle = acos( abs(point.y() - m_position.y())/(point-m_position).length() );

  if ( point.x() < m_position.x() ) { angle *= -1; }
  if ( point.y() > m_position.y() ) { angle = -(angle + M_PI); }

  if ( point.x() != m_position.x()
       || point.y() != m_position.y() )
    {
      m_direction = (point - m_position).normalized();
      m_angle = angle;
    }
}

/*virtual*/ void LevelActor::updateCurrentVertex()
{
  float bloc_size_w = m_core->window()->getSize().x/m_level->gridSize().x();
  float bloc_size_h = m_core->window()->getSize().y/m_level->gridSize().y();
  size_t i = m_position.y()/bloc_size_h;
  size_t j = m_position.x()/bloc_size_w;

  m_current_vertex = i * m_level->gridSize().y() + j;
}

/*virtual*/ void LevelActor::update()
{
  if(!m_alive) { return; }
  
  // compute angle
  {
    float deg = static_cast<float>(m_angle*180) / M_PI;
    m_shape.setRotation(deg);
  }

  if (m_current_life <= 0)
    {
      die();
      return;
    }

  m_health_bar->position(sk::Vector2f(m_position.x()
				      , m_position.y()
				      - m_radius));
  
  m_health_bar->current(m_current_life);
  m_health_bar->update();
  
  m_shot->update();
  LevelEntity::update();
}

/*virtual*/ void LevelActor::display()
{
  if(!m_alive) {return;}
  m_shot->display();
  LevelEntity::display();
  m_health_bar->display();
}

/*virtual*/ Shot* LevelActor::shot() const
{
  return m_shot.get();
}

/*virtual*/ void LevelActor::takeShot()
{
  m_current_life -= 1*m_max_life/4.0;
}

LevelActor::~LevelActor()
{
  
}
