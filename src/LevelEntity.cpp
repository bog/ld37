/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <LevelEntity.hpp>

LevelEntity::LevelEntity(Core* core, sk::Vector2f position)
  : Scene(core)
  , m_radius (16.0f)
  , m_position(position)
{
 
  radius(m_radius);
}

void LevelEntity::radius(float radius)
{
  m_radius = radius;
  m_shape.setOrigin(m_radius, m_radius);
}

/*virtual*/ void LevelEntity::update()
{
  m_shape.setPosition(sf::Vector2f(m_position.x()
				   , m_position.y()));
  m_shape.setRadius(m_radius);
}

/*virtual*/ void LevelEntity::display()
{
  m_core->window()->draw(m_shape);
}


LevelEntity::~LevelEntity()
{
  
}
