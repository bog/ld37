/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <FailureScene.hpp>

FailureScene::FailureScene(Core* core)
  : PictureScene(core, "failure")
{
}

/*virtual*/ void FailureScene::reset()
{
  //m_core->playMusic("enter_the_room");
  PictureScene::reset();
}


/*virtual*/ void FailureScene::update()
{
  if ( accept()
      && ready() )
    {
      m_core->goMain();
      m_clock.restart();
    }
}

FailureScene::~FailureScene()
{
  
}
