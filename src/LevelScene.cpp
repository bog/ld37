/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <LevelScene.hpp>
#include <Player.hpp>
#include <Goal.hpp>
#include <Wall.hpp>
#include <Baddy.hpp>
#include <Shot.hpp>
#include <SpawnPoint.hpp>
#include <LevelEntity.hpp>
#include <FWPath.hpp>
#include <random>
#include <chrono>

LevelScene::LevelScene(Core* core)
  : Scene (core)
  , m_grid_size ({9, 9})
  , m_max_baddies (10)
{
  // Add background.
  m_bg.setSize(sf::Vector2f(m_core->window()->getSize()));
  m_bg.setTexture(m_core->textures()->get("bg"));


  m_grid = createLevelGrid(m_grid_size.x(), m_grid_size.y());
  buildLevel(m_grid, 4);

  m_path = std::make_unique<FWPath>(this, m_grid);  
  m_path->compute();
}

sk::Vector2f LevelScene::vertexPosition(size_t vertex)
{
  size_t i = vertex/(int)m_grid_size.y();
  size_t j = vertex%(int)m_grid_size.x();
  sf::FloatRect area = m_grid[i][j].first;
  
  return {area.left + area.width/2, area.top + area.height/2};
}

bool LevelScene::endOfTheLevel()
{
  if (!m_player || !m_goal) { return false; }
  
  float dist = (m_player->position() - m_goal->position()).length();
  return m_baddies.empty()
    && m_goal->isOpen()
    && dist < m_player->radius() + m_goal->radius();
}

Player* LevelScene::player() const
{
  return m_player.get();
}

bool LevelScene::collision(LevelActor* actor, Wall* wall)
{
  /*
   * http://stackoverflow.com/questions/21089959/detecting-collision-of-rectangle-with-circle
   */
  
  float dist_x = abs(actor->position().x() - wall->position().x());
  float dist_y = abs(actor->position().y() - wall->position().y());
  
  if(dist_x > actor->radius() + wall->size().x()/2
     || dist_y > actor->radius() + wall->size().y()/2)
    {
      return false;
    }
  
  return true;
}

Wall* LevelScene::wallPointCollision (sk::Vector2f point)
{
  for(auto& wall : m_walls)
    {
      if ( !(point.y() < wall->position().y() - wall->size().y()/2
	     || point.y() > wall->position().y() + wall->size().y()/2
	     || point.x() < wall->position().x() - wall->size().x()/2
	     || point.x() > wall->position().x() + wall->size().x()/2) )
	{
	  return wall.get();
	}
    }

  return nullptr;
}

bool LevelScene::collision (LevelEntity* entity, sk::Vector2f point)
{
  return (entity->position() - point).length() <= entity->radius();
}

SpawnPoint* LevelScene::spawnPointCollision (sk::Vector2f point)
{
  for (auto& spawn : m_spawns)
    {
      if ( collision(spawn.get(), point) )
	{
	  return spawn.get();
	}
    }

  return nullptr;
}

LevelActor* LevelScene::actorPointCollision (sk::Vector2f point)
{
  for (auto& baddy : m_baddies)
    {
      if ( collision(baddy.get(), point) )
	{
	  return baddy.get();
	}
    }

  if ( (m_player->position() - point).length() <= m_player->radius() )
    {
      return m_player.get();
    }

  return nullptr;
}

bool LevelScene::lookingAt(LevelActor* actor, LevelActor* target)
{
  sk::Vector2f direction = actor->direction();
  float step_ratio = 1;
  sk::Vector2f point = actor->position();

  while( (point - target->position()).length() > 32 )
    {
      point += direction * step_ratio;
      
      LevelActor* actor_found = actorPointCollision(point);

      if(actor_found != nullptr
	 && actor_found == target)
  	{
  	  return true;
  	}
    }

  return false;
}

void LevelScene::shotInterception (Shot* shot)
{
  sk::Vector2f direction = shot->direction();
  float step_ratio = 1;
  sk::Vector2f point = shot->source();

  while( (point - shot->destination()).length() > 32 )
    {
      point += direction * step_ratio;
      
      Wall* wall = wallPointCollision(point);

      if(wall != nullptr)
  	{
	  spark(point);
  	  shot->destination(point);
  	  return;
  	}

      LevelActor* actor = actorPointCollision(point);

      // The actors musn't shoot theirself.
      if ( actor != nullptr && actor != shot->actor() )
  	{
	  // The baddies musn't shoot each others.
	  if ( actor == m_player.get()
	       || shot->actor() == m_player.get() )
	    {
	      blood(point);
	      shot->destination(point);
	      actor->takeShot();
	      return;
	    }
  	}

      SpawnPoint* spawn = spawnPointCollision(point);
      
      if ( spawn != nullptr && shot->actor() == m_player.get() )
	{
	  blood(point);
	  shot->destination(point);
	  spawn->takeShot();
	  return;
	}
    }
}

bool LevelScene::collision(LevelActor* actor)
{
  // With screen.
  if (actor->position().x() - actor->radius() < 0
      || actor->position().x() + actor->radius() > m_core->window()->getSize().x
      || actor->position().y() - actor->radius() < 0
      || actor->position().y() + actor->radius() > m_core->window()->getSize().y)
    {
      return true;
    }
  
  // With walls.
  for(auto& wall : m_walls)
    {
      if ( collision(actor, wall.get()))
	{
	  return true;
	}
    }
  return false;
}

void LevelScene::update()
{
  if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) )
    {
      m_core->goMain();
    }
    
  if (m_goal)
    {
      m_goal->update();
    }

  if (m_player)
    {
      m_player->update();
    }

  if ( m_spawns.empty()
       && m_baddies.empty()
       && m_goal )
    {
      m_goal->open();
    }
  
  if( endOfTheLevel() )
    {
      m_core->goEnd();
    }

  for(auto& wall : m_walls)
    {
      wall->update();
    }

  for(auto& baddy : m_baddies)
    {
      baddy->update();
    }
  
  for(auto& spawn : m_spawns)
    {
      spawn->update();
    }

  for(auto& anim : m_animations)
    {
      anim->update( m_core->framerate()->getInterval() );
    }
  
  // Remove elements in memory
  m_baddies.erase(remove_if(m_baddies.begin(), m_baddies.end(), [](auto& baddy){
	return !baddy->alive();
      }), m_baddies.end());

  m_spawns.erase(remove_if(m_spawns.begin(), m_spawns.end(), [](auto& spawn){
	return !spawn->alive();
      }), m_spawns.end());

  m_animations.erase(remove_if(m_animations.begin(), m_animations.end(), [](auto& anim){
	return anim->getStatus() != sk::AnimationStatus::Playing;
      }), m_animations.end());

  if( m_core->cheating() )
    {
      for(auto& spawn : m_spawns)
  	{
  	  spawn->spawnInterval(1500);
  	}
    }
}

void LevelScene::display()
{
  m_core->window()->draw(m_bg);

  if (m_goal)
    {
      m_goal->display();
    }

  for(auto& wall : m_walls)
    {
      wall->display();
    }

  for(auto& spawn : m_spawns)
    {
      spawn->display();
    }
    
  for(auto& baddy : m_baddies)
    {
      baddy->display();
    }

  if (m_player)
    {
      m_player->display();
    }

  for(auto& anim : m_animations)
    {
      m_core->window()->draw(*anim.get());
    }
}

void LevelScene::addBaddy(sk::Vector2f pos)
{
  m_baddies.push_back( std::make_unique<Baddy>(m_core, this, pos) );
}

LevelGrid LevelScene::createLevelGrid(size_t n_width, size_t n_height)
{
  LevelGrid grid;
  float const width = m_core->window()->getSize().x/static_cast<float>(n_width);
  float const height = m_core->window()->getSize().y/static_cast<float>(n_height);

  for(size_t i=0; i< n_height; i++)
    {
      std::vector<std::pair<sf::FloatRect, char>> line;
      
      for(size_t j=0; j< n_width; j++)
	{
	  sf::FloatRect area;
	  area.top = i * height;
	  area.left = j * width;
	  area.width = width;
	  area.height = height;
	  
	  line.push_back({area, '.'});
	}
      
      grid.push_back(line);
    }

  return grid;
}

void LevelScene::dumpGrid(LevelGrid& grid)
{  
  for(size_t i=0; i<grid.size(); i++)
    {
      for(size_t j=0; j<grid[i].size(); j++)
	{
	  std::cout << grid[i][j].second << " ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
  std::cout<<std::endl;
}

void LevelScene::clearLevel()
{
  m_player = nullptr;;
  m_goal = nullptr;
  m_walls.clear();
  m_baddies.clear();
  m_spawns.clear();
}

void LevelScene::buildLevel(LevelGrid& grid, int n_spawn_point)
{
  clearLevel();
  
  // Place play and goal.
  std::mt19937 rng(std::chrono::steady_clock::now().time_since_epoch().count());

  size_t player_i;
  size_t player_j;
  size_t goal_i;
  size_t goal_j;

  
  float tile_width = m_core->window()->getSize().x/grid[0].size();
  float tile_height = m_core->window()->getSize().y/grid.size();

  std::uniform_int_distribution<int> uid_width(0, grid[0].size()-1);
  std::uniform_int_distribution<int> uid_height(0, grid.size()-1);
  std::uniform_int_distribution<int> uid_wall_width(tile_width/2, tile_width);
  std::uniform_int_distribution<int> uid_wall_height(tile_height/2, tile_height);
  std::uniform_int_distribution<int> uid_spawn_interval(5000, 10000);
		  

  {
    std::uniform_int_distribution<int> uid_side(1, 4);
  
    switch( uid_side(rng) )
      {
      case 1 :
	player_i = 0;
	player_j = uid_width(rng);
	goal_i = grid.size()-1;
	goal_j = grid[0].size() - 1 - player_j;
	break;

      case 2 :
	player_i = uid_height(rng);
	player_j = grid[0].size()-1;
	goal_i = grid.size() - 1 - player_i;
	goal_j = 0;
	break;

      case 3 :
	player_i = grid.size()-1;
	player_j = uid_width(rng);
	goal_i = 0;
	goal_j = grid[0].size() - 1 - player_j;

	break;

      case 4 :
	player_i = uid_height(rng);
	player_j = 0;
	goal_i = grid.size() - 1 - player_i;
	goal_j = grid[0].size() - 1;
	break;
      }  
  }
  
  {
    sf::FloatRect p_area = grid[player_i][player_j].first;
    grid[player_i][player_j].second = 'p';
  
    m_player = std::make_unique<Player>(m_core
					, this
					, sk::Vector2f(p_area.left + p_area.width/2
						       , p_area.top + p_area.height/2) );

    sf::FloatRect g_area = grid[goal_i][goal_j].first;
    grid[goal_i][goal_j].second = 'g';
  
    m_goal = std::make_unique<Goal>(m_core
				    , sk::Vector2f(g_area.left + g_area.width/2
						   , g_area.top + g_area.height/2) );
  }
  
  // Place secure path using a walker.
  {
    int i = (int) player_i;
    int j = (int) player_j;
    
    while (i != (int) goal_i
	   || j != (int) goal_j)
    {
      int di = 0;
      int dj = 0;

      if (i < (int) goal_i) {di = 1;}
      else if (j < (int) goal_j) {dj = 1;}
      else if (i > (int) goal_i) {di = -1;}
      else if (j > (int) goal_j) {dj = -1;}
	  
      if ( i+di >= 0 && i+di < (int) grid.size() )
	{
	  i += di;
	}
      
      if ( j+dj >= 0 && j+dj < (int) grid[0].size() )
	{
	  j += dj;
	}

      if ( (i != (int) goal_i || j != (int) goal_j)
	   && (i != (int) player_i || j != (int) player_j) )
	{
	  grid[i][j].second = '#';
	}
    }
  }

  // Add SpawnPoints
  {
    int placed_spawn_point = 0;
    std::uniform_int_distribution<int> uid_dir(1, 4);
    int max_op = 1024;
    int op = 0;
    while (placed_spawn_point < n_spawn_point && op < max_op)
      {
	op++;
	size_t spawn_i = uid_height(rng);
	size_t spawn_j = uid_width(rng);

	if (grid[spawn_i][spawn_j].second == '.')
	  {
	    sf::FloatRect area = grid[spawn_i][spawn_j].first;
	    m_spawns.push_back(std::make_unique<SpawnPoint>(m_core
							    , this
							    , sk::Vector2f(area.left
									   + area.width/2
									   , area.top
									   + area.height/2)
							    , uid_spawn_interval(rng)));
	    grid[spawn_i][spawn_j].second = 's';

	    // Place a wall next to the spawn
	    {
	      sf::FloatRect area;
	      bool placed = false;
	      
	      for(int i=spawn_i-1; i<=(int)spawn_i+1; i++)
		{
		  for(int j=spawn_j-1; j<=(int)spawn_j+1; j++)
		    {
		      if (!placed && i>=0 && i<(int)grid.size()
			  && j>=0 && j<(int)grid[0].size()
			  && grid[i][j].second == '.')
			{
			  area = grid[i][j].first;
			  grid[i][j].second = 'w';
			  placed = true;
			}
		    }
		}

	      if (placed)
		{		  
		  m_walls.push_back(std::make_unique<Wall>(m_core
							   , sk::Vector2f(area.left
									  + area.width/2
									  , area.top
									  + area.height/2)
							   , sk::Vector2f(uid_wall_width(rng)
									  , uid_wall_height(rng)) ));
		}
	    }
	    
	    // Walk from the spawn to the secure path.
	    {
	      int i = (int) spawn_i;
	      int j = (int) spawn_j;
	      bool found_path = false;
	      
	      while (!found_path)
		{
		  int di = 0;
		  int dj = 0;
		  
		  switch ( uid_dir(rng) )
		    {
		    case 1:// up
		      di = -1;
		      break;
		      
		    case 2:// down 
		      di = 1;
		      break;
		      
		    case 3:// left
		      dj = -1;
		      break;

		    case 4:// right
		      dj = 1;
		      break;
		    }

		  if (i+di >=0 && i+di < (int) grid.size()
		      && j+dj >=0 && j+dj < (int) grid[0].size()
		      && grid[i+di][j+dj].second != 'w')
		    {
		      i += di;
		      j += dj;

		      if (grid[i][j].second == '#')
			{
			  found_path = true;
			}
		      
		      grid[i][j].second = '0' + placed_spawn_point;
		    }
		}
	    }// walker
	    
	    placed_spawn_point++;
	  }
      }
  }// spawns

  // Complete the level with walls.
  for(size_t i=0; i<grid.size(); i++)
    {
      for(size_t j=0; j<grid[i].size(); j++)
	{
	  if (grid[i][j].second == '.')
	    {
	      sf::FloatRect area = grid[i][j].first;
	      
	      m_walls.push_back(std::make_unique<Wall>(m_core
						       , sk::Vector2f(area.left
								      + area.width/2
								      , area.top
								      + area.height/2)
						       , sk::Vector2f(uid_wall_width(rng)
								      , uid_wall_height(rng)) ));
	      grid[i][j].second = 'w';

	    }
	}
    }
  
}

void LevelScene::blood(sk::Vector2f position)
{
  splash( position, sf::Color(150, 0, 0) );
}

void LevelScene::spark(sk::Vector2f position)
{
  splash( position, sf::Color(255, 255, 0) );
}

void LevelScene::splash(sk::Vector2f position, sf::Color color)
{
  std::unique_ptr<sk::Animation> anim = std::make_unique<sk::Animation>();
  for(int i=0; i<8; i++)
    {
      anim->add(*m_core->textures()->get("splash")
	     , {32*i, 0, 32, 32}
	     , 0.01f);
      
      anim->setColor(color);
    }
  
  anim->play();
  anim->setPosition(position.x() - anim->getLocalBounds().width/2
		    , position.y() - anim->getLocalBounds().height/2);
  
  m_animations.push_back(std::move(anim));
}

LevelScene::~LevelScene()
{
  
}
