/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <SpawnPoint.hpp>
#include <Baddy.hpp>
#include <HealthBar.hpp>

SpawnPoint::SpawnPoint(Core* core, LevelScene* level, sk::Vector2f position, float spawn_interval)
  : LevelEntity(core, position)
  , Killable(13 * LevelActor::MAX_LIFE)
  , m_spawn_interval(spawn_interval)
  , m_level(level)
{
  radius( (m_core->window()->getSize().x/m_level->gridSize().x())/4.0 );
  sf::Texture* t = m_core->textures()->get("spawn");
  m_shape.setTexture(t);

  float hb_height = m_radius*1.2;

  if (m_position.y() - hb_height < 0)
    {
      hb_height *= -1;
    }
  
  sk::Vector2f hb_pos(m_position.x()			
		      , m_position.y()-hb_height);


  m_health_bar = std::make_unique<HealthBar>(core
					     , hb_pos
					     , sk::Vector2f(m_radius*2
							    , m_radius/5)
					     , m_max_life);
}

/*virtual*/ void SpawnPoint::update()
{
  if (!m_alive) {return;}
  
  if (m_spawn_clock.getElapsedTime().asMilliseconds() >= m_spawn_interval)
    {
      spawn();
      m_spawn_clock.restart();
    }

  if (m_current_life <= 0) { die(); }
  
  m_health_bar->current(m_current_life);
  m_health_bar->update();
  
  LevelEntity::update();
}

/*virtual*/ void SpawnPoint::display()
{
  if (!m_alive) {return;}
  m_health_bar->display();
  LevelEntity::display();
}

/*virtual*/ void SpawnPoint::spawn()
{
  if ( !m_level->canAddBaddy() )
    {
      return;
    }
  
  m_level->addBaddy(m_position);
  
  m_current_life -= LevelActor::MAX_LIFE;
}

/*virtual*/ void SpawnPoint::takeShot()
{
  m_current_life -= m_max_life/13;
}

SpawnPoint::~SpawnPoint()
{
  
}
