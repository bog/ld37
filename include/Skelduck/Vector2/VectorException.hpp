/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef VECTOREXCEPTION_HPP
#define VECTOREXCEPTION_HPP
#include <iostream>

namespace sk
{
  struct VectorException : public std::exception
  {
    std::string m_msg;
    
  VectorException(std::string msg)
    : m_msg(msg) {}

    char const* what() const noexcept override
    {
      return m_msg.c_str();
    }
  };
}

#endif
