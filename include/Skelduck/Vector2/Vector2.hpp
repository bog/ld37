/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/


#ifndef VECTOR2_HPP
#define VECTOR2_HPP
#include <iostream>
#include <cmath>
#include "VectorException.hpp"
#include "VectorNullLength.hpp"
#include "VectorDivideByZero.hpp"

namespace sk
{
  template<typename T>
    class Vector2
    {
    public:
      // CONSTRUCTORS & DESTRUCTORS
      explicit Vector2()
	: Vector2(static_cast<T>(0), static_cast<T>(0)) {}

      explicit Vector2(T x, T y)
	: m_x (x)
	, m_y (y)
      {

      }

    Vector2(std::initializer_list<T> const& lst)
      : Vector2( *lst.begin(), *(lst.begin() + 1) )
	{
	}

      Vector2( Vector2 const& other ) // copy
	{
	  m_x = other.m_x;
	  m_y = other.m_y;
	}

      Vector2( Vector2 const&& other ) // move
	{
	  m_x = other.m_x;
	  m_y = other.m_y;
	}

      virtual ~Vector2(){}

      // ACCESSORS
      inline T x() const { return m_x; }
      inline T y() const { return m_y; }

      // MUTATORS
      inline void x(T _x) { m_x = _x; }
      inline void y(T _y) { m_y = _y; }

      // OPERATORS
      // assignement
      Vector2& operator=(Vector2 const& other)
	{
	  m_x = other.m_x;
	  m_y = other.m_y;

	  return *this;
	}

      // OPERATIONS
      T dot(Vector2 const& other) const
      {
	return m_x * other.m_x + m_y * other.m_y;
      }

      T length() const
      {
	return sqrt( m_x * m_x + m_y * m_y);
      }

      T angle(Vector2 const& other) const
      {
	return acos( ( dot(other) )/( length() * other.length() ) );
      }

      // TRANSFORMATIONS
      Vector2 normalized() const
      {
	T len = length();
	if(len == 0) { throw VectorNullLength(); }
	return Vector2(m_x/len, m_y/len);
      }

      void normalize()
      {
	T len = length();
	if(len == 0) { throw VectorNullLength(); }
	m_x /= len;
	m_y /= len;
      }

      Vector2 rotated(float angle) const
      {
	return Vector2(m_x * cos(angle) - m_y * sin(angle)
		       , m_x * sin(angle) + m_y * cos(angle) );

      }

      void rotate(float angle)
      {
	m_x = m_x * cos(angle) - m_y * sin(angle);
	m_y = m_x * sin(angle) + m_y * cos(angle);
      }

    protected:
      T m_x;
      T m_y;

    private:

    };


  // EXTERNALS
  template <typename T>
    std::ostream& operator<<(std::ostream& oss
			     , Vector2<T> const& vec)
    {
      oss << "(" << vec.x() << ", " << vec.y() << ")";
      return oss;
    }

  // operations
  template <typename T> // *
    Vector2<T> operator*(T const& lhs, Vector2<T> const& rhs)
    {
      return Vector2<T>( rhs.x() * lhs, rhs.y() * lhs );
    }

  template <typename T>
    Vector2<T> operator*(Vector2<T> const& lhs, T const& rhs)
    {
      return rhs * lhs;
    }

  template <typename T>
    void operator*=(Vector2<T>& lhs, T const& rhs)
    {
      lhs = lhs * rhs;
    }

  template <typename T> // /
    Vector2<T> operator/(Vector2<T> const& lhs, T const& rhs)
    {
      if(rhs == 0) { throw VectorDivideByZero(); }
      return lhs * (1/rhs);
    }

  template <typename T>
    void operator/=(Vector2<T>& lhs, T const& rhs)
    {
      lhs = lhs / rhs;
    }

  template <typename T> // +
    Vector2<T> operator+(Vector2<T> const& lhs, Vector2<T> const& rhs)
    {
      return Vector2<T>( lhs.x() + rhs.x(), lhs.y() + rhs.y() );
    }

  template <typename T>
    void operator+=(Vector2<T>& lhs, Vector2<T> const& rhs)
    {
      lhs = lhs + rhs;
    }

  template <typename T> // -
    Vector2<T> operator-(Vector2<T> const& lhs, Vector2<T> const& rhs)
    {
      return Vector2<T>( lhs.x() - rhs.x(), lhs.y() - rhs.y() );
    }

  template <typename T>
    void operator-=(Vector2<T>& lhs, Vector2<T> const& rhs)
    {
      lhs = lhs - rhs;
    }

  // TYPEDEF
  typedef Vector2<double> Vector2d;
  typedef Vector2<float> Vector2f;
  typedef Vector2<int> Vector2i;
  typedef Vector2<unsigned int> Vector2u;
}
#endif
