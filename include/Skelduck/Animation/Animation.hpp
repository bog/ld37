/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef ANIMATION_HPP
#define ANIMATION_HPP
#include <iostream>
#include <memory>
#include <vector>
#include <SFML/Graphics.hpp>
#include "AnimationNoFrame.hpp"
#include "Frame.hpp"

namespace sk
{
  enum class AnimationStatus
  {
    Playing
      , Paused 
      , Stopped
      };
    
  class Animation : public sf::Sprite
  {
  public:
    explicit Animation()
      : sf::Sprite()
      , m_elapsed (0.0f)
      , m_status (AnimationStatus::Stopped)
      , m_current_frame (0)
      , m_loop (false)
      , m_paused_time (0.0f)
    {
    }

    virtual ~Animation() {}

    inline AnimationStatus getStatus() const { return m_status; }

    inline void play()
    {
      if(m_status != AnimationStatus::Playing)
	{
	  m_elapsed = 0;
	}
      
      m_status = AnimationStatus::Playing;
      
      if( m_frames.empty() )
	{
	  throw AnimationNoFrame();
	}
      
      updateSprite();
    }

    inline void setLoop(bool loop) { m_loop = loop; }
    
    inline void pause()
    {
      m_status = AnimationStatus::Paused;
    }
    
    inline void stop()
    {
      m_status = AnimationStatus::Stopped;
      m_current_frame = 0;
      m_elapsed = 0;
    }
    
    void add(sf::Texture& texture, sf::IntRect rect, float duration)
    {
      Frame* frame = new Frame(texture);
      frame->rect = rect;
      frame->duration = duration;
      m_frames.push_back( std::unique_ptr<Frame>(frame) );
      
      updateSprite();
    }

    void add(sf::Texture& texture, float duration)
    {
      add(texture
	  , {0
	      , 0
	      , static_cast<int>(texture.getSize().x)
	      , static_cast<int>(texture.getSize().y)}
	  , duration);
    }
    
    void update(float dt)
    {
      if( m_status != AnimationStatus::Playing )
	{
	  return;
	}

      if( m_elapsed >= m_frames[m_current_frame]->duration )
	{
	  m_current_frame++;

	  if( m_current_frame >= m_frames.size() )
	    {
	      m_current_frame = 0;
	      if( !m_loop ) { stop(); }
	    }
	  
	  updateSprite();
	  
	  m_elapsed = 0;
	}

      m_elapsed += dt;
    }
    
  protected:
    float m_elapsed;
    AnimationStatus m_status;
    size_t m_current_frame;
    bool m_loop;
    float m_paused_time;
    
    void updateSprite()
    {
      setTexture( m_frames[m_current_frame]->texture );
      setTextureRect( m_frames[m_current_frame]->rect );
    }
    
    std::vector< std::unique_ptr<Frame> > m_frames;
  private:
    Animation( Animation const& animation ) = delete;
    Animation& operator=( Animation const& animation ) = delete;
  };
}
#endif
