/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef KEYBOARDCONTROL_HPP
#define KEYBOARDCONTROL_HPP
#include <iostream>
#include "Control.hpp"
#include <SFML/Graphics.hpp>

namespace sk
{
  class KeyboardControl : public Control
  {
  public:
  KeyboardControl(sf::Keyboard::Key key)
    : m_key(key)
    {

    }

    virtual bool isActive() const override
    {
      return sf::Keyboard::isKeyPressed(m_key);
    }

    virtual float value() const override
    {
      return isActive();
    }
  protected:
    sf::Keyboard::Key m_key;
  };
}

#endif
