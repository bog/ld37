/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef JOYSTICKAXISCONTROL_HPP
#define JOYSTICKAXISCONTROL_HPP
#include <iostream>
#include <SFML/Graphics.hpp>
#include "Control.hpp"

namespace sk
{
  class JoystickAxisControl : public Control
  {
  public:
  JoystickAxisControl(unsigned int joystick
		      , sf::Joystick::Axis axis
		      , float threshold)
    : m_joystick (joystick)
      , m_axis (axis)
      , m_threshold (threshold)
    {

    }

    virtual bool isActive() const override
    {
      float axis_value;

      axis_value = sf::Joystick::getAxisPosition(m_joystick, m_axis);

      if( abs(axis_value) <= abs(m_threshold) )
	{
	  return false;
	}
      return true;
    }

    virtual float value() const override
    {
      float axis_value;

      axis_value = sf::Joystick::getAxisPosition(m_joystick, m_axis);

      return ( isActive()
	       ? axis_value
	       : 0 );
    }
  protected:
    unsigned int m_joystick;
    sf::Joystick::Axis m_axis;
    float m_threshold;
  };
}
#endif
