/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CONTROL_HPP
#define CONTROL_HPP
#include <iostream>

namespace sk
{
  enum XboxButton
  {
    A = 0
    , B = 1
    , X = 2
    , Y = 3
  };

  class Control
  {
  public:
    explicit Control() { }
    virtual ~Control() {}

    virtual float value() const = 0;
    virtual bool isActive() const = 0;

  protected:

  private:
    Control( Control const& control ) = delete;
    Control& operator=( Control const& control ) = delete;
  };
}
#endif
