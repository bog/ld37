/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef JOYSTICKBUTTONCONTROL_HPP
#define JOYSTICKBUTTONCONTROL_HPP
#include <iostream>
#include <SFML/Graphics.hpp>
#include "Control.hpp"

namespace sk
{
  class JoystickButtonControl : public Control
  {
  public:
  JoystickButtonControl(unsigned int joystick, unsigned int button)
    : m_joystick (joystick)
      , m_button (button)
    {

    }

    virtual bool isActive() const override
    {
      return sf::Joystick::isButtonPressed(m_joystick, m_button);
    }

    virtual float value() const override
    {
      return isActive();
    }

  protected:
    unsigned int m_joystick;
    unsigned int m_button;
  };
}

#endif
