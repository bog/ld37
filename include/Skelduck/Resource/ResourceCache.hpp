/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef RESOURCECACHE_HPP
#define RESOURCECACHE_HPP
#include <iostream>
#include <memory>
#include <unordered_map>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "IResource.hpp"
#include "ResourceAlreadyExists.hpp"
#include "ResourceDoesntExists.hpp"

namespace sk
{
  template<typename T>
    class ResourceCache
    {
    public:
      explicit ResourceCache() {}
      virtual ~ResourceCache()
	{
	  for(auto& rsc : m_resources)
	    {
	      (rsc.second)->release();
	    }
	}

      void add(std::string name, IResource<T>* resource)
      {
	auto itr = m_resources.find(name);

	if( itr != m_resources.end() )
	  {
	    throw ResourceAlreadyExists(name);
	  }

	resource->load();

	m_resources.insert(std::make_pair(name, std::unique_ptr<IResource<T>>(resource) ));
      }

      T* get(std::string name)
      {
      	auto itr = m_resources.find(name);

      	if( itr == m_resources.end() )
      	  {
      	    throw ResourceDoesntExists(name);
      	  }

      	return m_resources[name]->get();
      }

    protected:
      std::unordered_map<std::string, std::unique_ptr<IResource<T>>> m_resources;

    private:
      ResourceCache( ResourceCache const& resourcecache ) = delete;
      ResourceCache& operator=( ResourceCache const& resourcecache ) = delete;
    };
}
#endif
