/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef RESOURCEDOESNTEXISTS_HPP
#define RESOURCEDOESNTEXISTS_HPP
#include <iostream>

namespace sk
{
  class ResourceDoesntExists : public std::exception
  {
  public:
  ResourceDoesntExists(std::string rsc_name)
    : m_msg( "Resource '" + rsc_name + "' does not exists.\n") {}

    char const* what() const noexcept override
    {
      return m_msg.c_str();
    }

  private:
    std::string m_msg;
  };
}

#endif
