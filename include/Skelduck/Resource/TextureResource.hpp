/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef TEXTURERESOURCE_HPP
#define TEXTURERESOURCE_HPP
#include <iostream>
#include <SFML/Graphics.hpp>
#include "IResource.hpp"

namespace sk
{
  class TextureResource : public IResource<sf::Texture>
  {
  public:
  TextureResource(std::string path) : m_path(path) {}
    virtual void load() override
    {
      m_texture = std::make_unique<sf::Texture>();
      m_texture->loadFromFile(m_path);
    }

    virtual sf::Texture* get() const {return m_texture.get();}
  private:
    std::string m_path;
    std::unique_ptr<sf::Texture> m_texture;
  };
}

#endif
