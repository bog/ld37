/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MUSICRESOURCE_HPP
#define MUSICRESOURCE_HPP
#include <iostream>
#include <SFML/Audio.hpp>
#include "IResource.hpp"

namespace sk
{
  class MusicResource : public IResource<sf::Music>
  {
  public:
  MusicResource(std::string path) : m_path(path) {}
    virtual void load() override
    {
      m_music = std::make_unique<sf::Music>();
      m_music->openFromFile(m_path);
    }

    virtual sf::Music* get() const
    {
      return m_music.get();
    }
  private:
    std::string m_path;
    std::unique_ptr<sf::Music> m_music;
  };
}

#endif
