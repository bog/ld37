/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef ANIMATIONSLOT_HPP
#define ANIMATIONSLOT_HPP
#include <iostream>
#include "Slot.hpp"
#include "../Animation/Animation.hpp"

namespace sk
{
  class AnimationSlot : public Slot<>
  {
  public:
    AnimationSlot(sk::Animation* anim)
      : m_animation (anim)
      {
      }
    
    ~AnimationSlot() { }
    
    virtual void execute() override
    {
      m_animation->play();
    }
  protected:
    sk::Animation* m_animation;
    
  private:
    AnimationSlot( AnimationSlot const& animationslot ) = delete;
    AnimationSlot& operator=( AnimationSlot const& animationslot ) = delete;
  };
}
#endif
