/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef SOUNDSLOT_HPP
#define SOUNDSLOT_HPP
#include <iostream>
#include <SFML/Audio.hpp>
#include "Slot.hpp"

namespace sk
{
  class SoundSlot : public Slot<>
  {
  public:
  SoundSlot(sf::SoundBuffer* sb)
    : m_sound_buffer (sb)
    {
      m_sound.setBuffer(*m_sound_buffer);
    }
  
    virtual ~SoundSlot() { }

    virtual void execute() override
    {
      m_sound.play();
    }
  
  protected:
    sf::SoundBuffer* m_sound_buffer;
    sf::Sound m_sound;
    
  private:
    SoundSlot( SoundSlot const& soundslot ) = delete;
    SoundSlot& operator=( SoundSlot const& soundslot ) = delete;
  };
}
#endif
