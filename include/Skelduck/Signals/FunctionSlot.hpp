/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef FUNCTIONSLOT_HPP
#define FUNCTIONSLOT_HPP
#include <iostream>
#include <functional>
#include "Slot.hpp"

namespace sk
{
  template<typename... P>
  class FunctionSlot : public Slot<P...>
  {
  public:
  FunctionSlot(std::function<void(P...)> fct)
    : m_function (fct)
    {
    }
    
    virtual ~FunctionSlot() {}

    virtual void execute(P... args) 
    {
      m_function(args...);
    }
    
  protected:
    std::function<void(P...)> m_function;
    
  private:
    FunctionSlot( FunctionSlot const& functionslot ) = delete;
    FunctionSlot& operator=( FunctionSlot const& functionslot ) = delete;
  };
}
#endif
