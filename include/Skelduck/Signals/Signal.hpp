/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef SIGNAL_HPP
#define SIGNAL_HPP
#include <iostream>
#include <vector>
#include "Slot.hpp"

namespace sk
{
  template<typename... P>
    class Signal
    {
    public:
      Signal() { }
      virtual ~Signal() { }
  
      virtual void connect(Slot<P...>* slot)
      {
	m_slots.push_back(slot);
      }
  
      virtual void fire(P... args)
      {
	for(Slot<P...>* slot : m_slots)
	  {
	    slot->execute(args...);
	  }
      }
  
    protected:
      std::vector<Slot<P...>*> m_slots;
  
    private:
      Signal( Signal const& signal ) = delete;
      Signal& operator=( Signal const& signal ) = delete;
    };
}
#endif
