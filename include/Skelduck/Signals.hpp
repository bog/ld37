#include "Signals/Signal.hpp"

#include "Signals/Slot.hpp"
#include "Signals/SoundSlot.hpp"
#include "Signals/AnimationSlot.hpp"
#include "Signals/FunctionSlot.hpp"
