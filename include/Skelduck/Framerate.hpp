/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef FRAMERATE_HPP
#define FRAMERATE_HPP
#include <iostream>
#include <SFML/Graphics.hpp>

namespace sk
{
  class Framerate
  {
  public:
    explicit Framerate() : m_dt(0) {}
    virtual ~Framerate() {}

    void begin()
    {
      m_clock.restart();
    }

    float getInterval() const { return m_dt; }
    float getFps() const { return 1 / m_dt; }
    
    void end()
    {
      m_dt = m_clock.getElapsedTime().asSeconds();
    }
    
  protected:
    sf::Clock m_clock;
    float m_dt;
    
  private:
    Framerate( Framerate const& framerate ) = delete;
    Framerate& operator=( Framerate const& framerate ) = delete;
  };
}
#endif
