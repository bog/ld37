/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef LEVELSCENE_HPP
#define LEVELSCENE_HPP
#include <iostream>
#include <Scene.hpp>
#include <vector>

class Player;
class Goal;
class Wall;
class Baddy;
class LevelActor;
class Shot;
class SpawnPoint;
class LevelEntity;
class FWPath;

using LevelGrid = std::vector< std::vector<std::pair<sf::FloatRect, char>> > ;

class LevelScene : public Scene
{
 public:
  explicit LevelScene(Core* core);
  virtual ~LevelScene();

  virtual void update();
  virtual void display();

  bool endOfTheLevel();
  bool collision(LevelActor* actor, Wall* wall);
  bool collision(LevelActor* actor);
  bool collision (LevelEntity* entity, sk::Vector2f point);
  
  Wall* wallPointCollision (sk::Vector2f point);
  LevelActor* actorPointCollision (sk::Vector2f point);
  SpawnPoint* spawnPointCollision (sk::Vector2f point);
  bool lookingAt(LevelActor* actor, LevelActor* target);
  void shotInterception (Shot* shot);
  
  Player* player() const;

  void addBaddy(sk::Vector2f pos);

  inline LevelGrid* grid() { return &m_grid; }
  inline FWPath* path() { return m_path.get(); }
  inline sk::Vector2f gridSize() const { return m_grid_size; }
  inline int maxBaddies() const { return m_max_baddies; }
  inline size_t nBaddies() const { return m_baddies.size(); }
  inline bool canAddBaddy() const { return (int)m_baddies.size() < m_max_baddies; }
  
  void dumpGrid(LevelGrid& grid);
  sk::Vector2f vertexPosition(size_t vertex);
  
 protected:
  std::unique_ptr<Player> m_player;
  std::unique_ptr<Goal> m_goal;
  std::vector<std::unique_ptr<Wall>> m_walls;
  std::vector<std::unique_ptr<Baddy>> m_baddies;
  std::vector<std::unique_ptr<SpawnPoint>> m_spawns;
  sf::RectangleShape m_bg;

  std::vector<std::unique_ptr<sk::Animation>> m_animations;
  LevelGrid m_grid;
  sk::Vector2f m_grid_size;
  
  std::unique_ptr<FWPath> m_path;

  int m_max_baddies;
  
  LevelGrid createLevelGrid(size_t n_width, size_t n_height);
  void clearLevel();
  void buildLevel(LevelGrid& grid, int n_spawn_point);

  void blood(sk::Vector2f position);
  void spark(sk::Vector2f position);
  void splash(sk::Vector2f position, sf::Color color);
  
 private:
  LevelScene( LevelScene const& levelscene ) = delete;
  LevelScene& operator=( LevelScene const& levelscene ) = delete;
};

#endif
