/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CORE_HPP
#define CORE_HPP
#include <iostream>
#include <memory>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <Skelduck/Skelduck.hpp>

#ifndef M_PI
#define M_PI 3.14159265359
#endif

class Scene;
class LevelScene;
class EndScene;
class FailureScene;
class MainScene;
class HelpScene;

class Core
{
 public:
  explicit Core(sf::RenderWindow* window);
  virtual ~Core();

  void update();
  void display();

  void run();

  inline sf::RenderWindow* window() const { return m_window; }
  
  inline sk::Vector2f mouse() const
  {    
    sf::Vector2i sfcoords = sf::Mouse::getPosition(*m_window);
    return sk::Vector2f(sfcoords.x, sfcoords.y);
  }

  inline sk::ResourceCache<sf::Texture>* textures () const
  {
    return m_textures.get();
  }

  inline sk::ResourceCache<sf::SoundBuffer>* soundBuffers () const
  {
    return m_sound_buffers.get();
  }

  inline sk::ResourceCache<sf::Music>* musics () const
  {
    return m_musics.get();
  }

  inline sk::Framerate* framerate() { return &m_framerate; }
  
  inline sf::Music* currentMusic() {return m_current_music;}
  inline void currentMusic(sf::Music* music) {m_current_music = music;}

  inline void playMusic (std::string name)
  {
    m_current_music = m_musics->get(name);
    m_current_music->setLoop(true);
    m_current_music->play();
  }
  
  inline bool cheating () const
  {
    return sf::Keyboard::isKeyPressed(sf::Keyboard::B)
      && sf::Keyboard::isKeyPressed(sf::Keyboard::O)
      && sf::Keyboard::isKeyPressed(sf::Keyboard::G);

  }
  
  void goLevel();
  void goEnd();
  void goFailure();
  void goMain();
  void goHelp();
  void playRandomGameMusic();
  
 protected:
  sf::RenderWindow* m_window;
  Scene* m_current_scene;
  std::unique_ptr<Scene> m_level_scene;
  std::unique_ptr<Scene> m_end_scene;
  std::unique_ptr<Scene> m_failure_scene;
  std::unique_ptr<Scene> m_main_scene;
  std::unique_ptr<Scene> m_help_scene;
  
  std::unique_ptr<sk::ResourceCache<sf::Texture>> m_textures;
  std::unique_ptr<sk::ResourceCache<sf::SoundBuffer>> m_sound_buffers;
  std::unique_ptr<sk::ResourceCache<sf::Music>> m_musics;
  
  sf::Music* m_current_music;
  
  sk::Framerate m_framerate;
  
  void go(std::unique_ptr<Scene>& scene);
  void reset(std::unique_ptr<Scene>& scene);
  
  void loadResources();
 private:
  Core( Core const& core ) = delete;
  Core& operator=( Core const& core ) = delete;
};

#endif
