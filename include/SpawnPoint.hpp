/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef SPAWNPOINT_HPP
#define SPAWNPOINT_HPP
#include <iostream>
#include <Core.hpp>
#include <LevelEntity.hpp>
#include <Killable.hpp>

class Baddy;
class LevelScene;
class HealthBar;

class SpawnPoint : public LevelEntity, public Killable
{
 public:
  explicit SpawnPoint(Core* core, LevelScene* level, sk::Vector2f position, float spawn_interval);
  virtual ~SpawnPoint();

  virtual void update() override;
  virtual void display() override;
  virtual void spawn();
  virtual void takeShot() override;

  inline virtual void spawnInterval(float si) { m_spawn_interval = si; }
  
 protected:
  float m_spawn_interval;
  sf::Clock m_spawn_clock;
  LevelScene* m_level;
  std::unique_ptr<HealthBar> m_health_bar;
  
 private:
  SpawnPoint( SpawnPoint const& spawnpoint ) = delete;
  SpawnPoint& operator=( SpawnPoint const& spawnpoint ) = delete;
};

#endif
