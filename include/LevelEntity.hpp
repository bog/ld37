/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef LEVELENTITY_HPP
#define LEVELENTITY_HPP
#include <iostream>
#include <Scene.hpp>

class LevelEntity : public Scene
{
 public:
  explicit LevelEntity(Core* core, sk::Vector2f position);
  virtual ~LevelEntity();

  virtual void update() override;
  virtual void display() override;
  
  virtual void radius(float radius);
  inline virtual float radius() const { return m_radius; }
  
  inline virtual sk::Vector2f position() const { return m_position; }
  
 protected:
  sf::CircleShape m_shape;
  float m_radius;
  sk::Vector2f m_position;
  
 private:
  LevelEntity( LevelEntity const& levelentity ) = delete;
  LevelEntity& operator=( LevelEntity const& levelentity ) = delete;
};

#endif
