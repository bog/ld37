/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GOAL_HPP
#define GOAL_HPP
#include <iostream>
#include <LevelEntity.hpp>

class Goal : public LevelEntity
{
 public:
  explicit Goal(Core* core, sk::Vector2f position);
  virtual ~Goal();

  virtual void update() override;
  virtual void display() override;

  void open();
  void close();

  inline bool isOpen() const {return m_open;}
  
 protected:
  bool m_open;
  
 private:
  Goal( Goal const& goal ) = delete;
  Goal& operator=( Goal const& goal ) = delete;
};

#endif
