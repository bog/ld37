/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef PICTURESCENE_HPP
#define PICTURESCENE_HPP
#include <iostream>
#include <Scene.hpp>

class PictureScene : public Scene
{
 public:
  explicit PictureScene(Core* core, std::string picture_name);
  virtual ~PictureScene();

  virtual void display() override;
  virtual void reset();

  virtual bool ready();
  virtual bool accept();
  
 protected:
  sf::RectangleShape m_bg;
  sf::Clock m_clock;
  float m_delay;

 private:
  PictureScene( PictureScene const& picturescene ) = delete;
  PictureScene& operator=( PictureScene const& picturescene ) = delete;
};

#endif
