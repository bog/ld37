/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef PLAYER_HPP
#define PLAYER_HPP
#include <iostream>
#include <LevelActor.hpp>

class Player : public LevelActor
{
 public:
  explicit Player(Core* core, LevelScene* level, sk::Vector2f position);
  virtual ~Player();

  virtual void update() override;
  virtual void display() override;
  void die () override;
  virtual void takeShot() override;
 protected:
  
 private:
  Player( Player const& player ) = delete;
  Player& operator=( Player const& player ) = delete;
};

#endif
