/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef WALL_HPP
#define WALL_HPP
#include <iostream>
#include <Scene.hpp>

class Wall : public Scene
{
 public:
  explicit Wall(Core* core, sk::Vector2f pos, sk::Vector2f size);
  virtual ~Wall();

  virtual void update() override;
  virtual void display() override;

  virtual sk::Vector2f position() const { return m_position; }
  virtual sk::Vector2f size() const { return m_size; }
  
 protected:
  sf::RectangleShape m_shape;
  sk::Vector2f m_position;
  sk::Vector2f m_size;
  
 private:
  Wall( Wall const& wall ) = delete;
  Wall& operator=( Wall const& wall ) = delete;
};

#endif
