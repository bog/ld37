/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef BADDY_HPP
#define BADDY_HPP
#include <iostream>
#include <LevelActor.hpp>


class Baddy : public LevelActor
{
 public:
  explicit Baddy(Core* core, LevelScene* level, sk::Vector2f position);
  virtual ~Baddy();

  virtual void update() override;
  virtual void display() override;
  virtual sk::Vector2f targetPoint (sk::Vector2f target, float accuracy);
  virtual void shootPlayer();
  virtual void takeShot() override;

 protected:

  float m_max_dist_player;
  float m_accuracy;
  size_t m_current_step;
  
 private:
  Baddy( Baddy const& baddy ) = delete;
  Baddy& operator=( Baddy const& baddy ) = delete;
};

#endif
