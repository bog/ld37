/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef SHOT_HPP
#define SHOT_HPP
#include <iostream>
#include <Scene.hpp>

class LevelScene;
class LevelActor;

class Shot : public Scene
{
 public:
  explicit Shot(Core* core, LevelScene* level, LevelActor* actor, float cooldown);
  explicit Shot(Core* core, LevelScene* level, LevelActor* actor);
  virtual ~Shot();

  virtual void update() override;
  virtual void display() override;

  inline sk::Vector2f source() const { return m_source; }
  inline sk::Vector2f destination() const { return m_destination; }
  inline void destination(sk::Vector2f dest)  { m_destination = dest; }
  inline sk::Vector2f direction() const { return (m_destination - m_source).normalized(); }
  
  LevelActor* actor();
    
  void fire(sk::Vector2f source, sk::Vector2f destination);
  
 protected:
  LevelScene* m_level;
  float m_fire_time;
  sf::Clock m_fire_clock;
  bool m_shooting;
  
  sk::Vector2f m_source;
  sk::Vector2f m_destination;
  sf::VertexArray m_array;

  float m_range;
  float m_cooldown_time;
  sf::Clock m_cooldown_clock;

  LevelActor* m_actor;
  sf::Sound m_sound;
 private:
  Shot( Shot const& shot ) = delete;
  Shot& operator=( Shot const& shot ) = delete;
};

#endif
