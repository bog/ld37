/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef FWPATH_HPP
#define FWPATH_HPP
#include <iostream>
#include <LevelScene.hpp>

class FWPath
{
 public:
  static const int MAX_COST;
  
  explicit FWPath(LevelScene* level, LevelGrid& grid);
  virtual ~FWPath();

  void compute();

  inline std::vector<std::vector<int>> paths () const { return m_paths; }
  inline std::vector<std::vector<float>> costs () const { return m_costs; }
  
 protected:
  LevelScene* m_level;
  std::vector<std::vector<float>> m_costs;
  std::vector<std::vector<int>> m_paths;
  size_t m_n_vertex;
  
 private:
  FWPath( FWPath const& fwpath ) = delete;
  FWPath& operator=( FWPath const& fwpath ) = delete;
};

#endif
