/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef HELPSCENE_HPP
#define HELPSCENE_HPP
#include <iostream>
#include <PictureScene.hpp>

class HelpScene : public PictureScene
{
 public:
  explicit HelpScene(Core* core);
  virtual ~HelpScene();

  virtual void reset() override;
  virtual void update() override;
 protected:
  
 private:
  HelpScene( HelpScene const& helpscene ) = delete;
  HelpScene& operator=( HelpScene const& helpscene ) = delete;
};

#endif
