/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef LEVELACTOR_HPP
#define LEVELACTOR_HPP
#include <iostream>
#include <LevelEntity.hpp>
#include <LevelScene.hpp>
#include <Killable.hpp>

class Shot;
class HealthBar;

class LevelActor : public LevelEntity, public Killable
{
 public:
  static const float MAX_LIFE;
  
  explicit LevelActor(Core* core, LevelScene* level, sk::Vector2f position);
  virtual ~LevelActor();

  virtual void updateCurrentVertex();
  virtual void update() override;
  virtual void display() override;
  virtual void move(float x_vel, float y_vel);
  void lookAt(sk::Vector2f point);
  virtual void takeShot() override;
  virtual Shot* shot() const;

  virtual inline sk::Vector2f direction() const { return m_direction; }
  virtual inline size_t currentVertex() const { return m_current_vertex; }
  
 protected:
  LevelScene* m_level;
  float m_velocity;
  float m_angle;
  size_t m_current_vertex;
  
  sk::Vector2f m_direction;

  std::unique_ptr<Shot> m_shot;

  std::unique_ptr<HealthBar> m_health_bar;
  
 private:
  LevelActor( LevelActor const& levelactor ) = delete;
  LevelActor& operator=( LevelActor const& levelactor ) = delete;
};

#endif
