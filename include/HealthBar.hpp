/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef HEALTHBAR_HPP
#define HEALTHBAR_HPP
#include <iostream>
#include <Scene.hpp>

class HealthBar : public Scene
{
 public:
  explicit HealthBar(Core* core, sk::Vector2f position, sk::Vector2f size, float max);
  virtual ~HealthBar();

  virtual void update() override;
  virtual void display() override;

  inline void position(sk::Vector2f position) { m_position = position; }
  inline void current(float current) { m_current = current; }
  
 protected:
  float m_max;
  float m_current;
  sf::RectangleShape m_shape;
  sf::RectangleShape m_bar;
  sk::Vector2f m_position;
  sk::Vector2f m_size;
  
 private:
  HealthBar( HealthBar const& healthbar ) = delete;
  HealthBar& operator=( HealthBar const& healthbar ) = delete;
};

#endif
